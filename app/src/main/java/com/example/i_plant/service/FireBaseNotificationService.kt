package com.example.i_plant.service

import com.google.firebase.messaging.FirebaseMessagingService
import timber.log.Timber

class FireBaseNotificationService : FirebaseMessagingService() {

    override fun onNewToken(token: String) {
        Timber.i("Firebase token:$token")
    }

}