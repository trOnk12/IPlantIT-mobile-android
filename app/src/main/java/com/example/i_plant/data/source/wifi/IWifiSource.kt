package com.example.i_plant.data.source.wifi

import com.example.i_plant.domain.model.IPlantAccessPoint
import kotlinx.coroutines.flow.Flow


// Implementation of WiFiSource could change in the future, due to deprecated WifiManager.startScan() method and possible being removed from the API.
interface IWifiSource {
    fun scanNearbyAccessPointDevices(): Flow<List<IPlantAccessPoint>>
}




