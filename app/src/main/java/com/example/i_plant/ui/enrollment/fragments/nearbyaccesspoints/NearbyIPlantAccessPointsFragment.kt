package com.example.i_plant.ui.enrollment.fragments.accesspoints

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.i_plant.R
import com.example.i_plant.core.base.BindingFragment
import com.example.i_plant.core.config.TimberTags
import com.example.i_plant.core.extension.observe
import com.example.i_plant.core.extension.showToast
import com.example.i_plant.core.helpers.TimberHelper
import com.example.i_plant.databinding.FragmentNearbByIplantAccessPointsBinding
import com.example.i_plant.domain.model.IPlantAccessPoint
import com.example.i_plant.ui.enrollment.fragments.accesspoints.adapter.IPlantAccessPointAdapter
import com.example.i_plant.ui.enrollment.viewmodel.EnrollmentFailure
import com.example.i_plant.ui.enrollment.viewmodel.EnrollmentViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.android.viewmodel.ext.android.sharedViewModel
import timber.log.Timber

@ExperimentalCoroutinesApi
class NearbyIPlantAccessPointsFragment :
    BindingFragment<FragmentNearbByIplantAccessPointsBinding>() {

    private val iPlantAccessPointAdapter: IPlantAccessPointAdapter =
        IPlantAccessPointAdapter { iPlantAccessPoint ->
            adapterOnClick(iPlantAccessPoint)
        }

    private val enrollmentViewModel: EnrollmentViewModel by sharedViewModel()

    override fun inflateBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) = DataBindingUtil.inflate(
        inflater,
        R.layout.fragment_nearb_by_iplant_access_points,
        container,
        false
    ) as FragmentNearbByIplantAccessPointsBinding

    override fun onAttach(context: Context) {
        super.onAttach(context)

        enrollmentViewModel.searchNearbyIPlantAccessPointDevices()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        observe(enrollmentViewModel.failure, ::onFailure)
        observe(enrollmentViewModel.iPlantAccessPoints, ::onIPlantAccessDevicesChange)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)

        with(binding) {
            with(nearbyIPlantAccessPointsRecyclerView) {
                layoutManager = LinearLayoutManager(requireContext())
                adapter = iPlantAccessPointAdapter
            }
        }

        return binding.root
    }

    private fun onIPlantAccessDevicesChange(iPlantAccessPoints: List<IPlantAccessPoint>) {
        iPlantAccessPointAdapter.updateAccessPointDevices(iPlantAccessPoints)
    }

    private fun onFailure(failure: EnrollmentFailure) {
        when (failure) {
            is EnrollmentFailure.WifiScanError -> showToast("Something when wrong while scanning wifi network")
            else -> showToast("Something unexpected happend.")
        }
    }

    private fun adapterOnClick(iPlantAccessPoint: IPlantAccessPoint) {
        TimberHelper.enrollmentTag("$iPlantAccessPoint has been clicked")

        enrollmentViewModel.connectToAccessPoint(iPlantAccessPoint)
    }

}
