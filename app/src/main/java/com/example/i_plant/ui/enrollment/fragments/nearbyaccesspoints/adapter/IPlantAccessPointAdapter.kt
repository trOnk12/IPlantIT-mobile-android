package com.example.i_plant.ui.enrollment.fragments.accesspoints.adapter

import android
.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.i_plant.databinding.ItemViewIPlantAccesPointBinding
import com.example.i_plant.domain.model.IPlantAccessPoint

class IPlantAccessPointAdapter(private val iPlantAccessPointOnClick: (IPlantAccessPoint) -> Unit) :
    RecyclerView.Adapter<IPlantAccessPointAdapter.ViewHolder>() {

    private val accessPointDevices: ArrayList<IPlantAccessPoint> = arrayListOf()

    fun updateAccessPointDevices(accessPointDevices: List<IPlantAccessPoint>) {
        val diffResult = DiffUtil.calculateDiff(
            IPlantAccessPointDiffUtil(
                accessPointDevices,
                this.accessPointDevices
            )
        )
        this.accessPointDevices.clear()
        this.accessPointDevices.addAll(accessPointDevices)
        diffResult.dispatchUpdatesTo(this)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemViewIPlantAccesPointBinding.inflate(inflater)

        return ViewHolder(binding, iPlantAccessPointOnClick)
    }

    override fun getItemCount() = accessPointDevices.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(accessPointDevices[position])

    class ViewHolder(
        private val binding: ItemViewIPlantAccesPointBinding,
        val iPlantAccessPointOnClick: (IPlantAccessPoint) -> Unit
    ) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(iPlantAccessPoint: IPlantAccessPoint) {
            binding.iPlantAccessPoint = iPlantAccessPoint
            binding.root.setOnClickListener { iPlantAccessPointOnClick(iPlantAccessPoint) }
            binding.executePendingBindings()
        }

    }

}


class IPlantAccessPointDiffUtil(
    private val newList: List<IPlantAccessPoint>,
    private val oldList: List<IPlantAccessPoint>
) : DiffUtil.Callback() {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].id == newList[newItemPosition].id
    }

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition] == newList[newItemPosition]
    }

}