package com.example.i_plant

import android.app.Application
import com.example.i_plant.di.wifiModule
import com.example.i_plant.ui.enrollment.viewModelModule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

import timber.log.Timber.DebugTree


class IPlantApplication : Application() {

    @ExperimentalCoroutinesApi
    override fun onCreate() {
        super.onCreate()

        startTimber()
        startKoin()
    }

    private fun startTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(DebugTree())
        }
    }

    private fun startKoin() {
        startKoin {
            androidContext(this@IPlantApplication)

            modules(
                wifiModule,
                viewModelModule
            )
        }
    }

}