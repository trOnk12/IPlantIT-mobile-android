package com.example.i_plant.core.helpers

import com.example.i_plant.core.config.TimberTags
import timber.log.Timber


class TimberHelper {
    companion object {
        fun enrollmentTag(message: String) {
            Timber.tag(TimberTags.I_PLANT_ENROLLMENT).d(message)
        }
    }
}